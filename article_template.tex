% Article, with default font size of 12pt.
\documentclass[12pt]{article}

% Additional packages being used
\usepackage{setspace}	% For line spacing
\usepackage{fontspec}	% For specifying fonts
\usepackage{titlesec}	% For custom titles
\usepackage{titling}	% For changing font of titles
\usepackage{enumitem}	% For custom list settings
\usepackage{hyperref}	% For hyperlinks

% Set paragraph format
\onehalfspacing	% 1.5 line spacing
\raggedbottom	% Don't justify last line's
\setlength{\parskip}{1em}	% Gap between paragraphs
\setlist[itemize]{nolistsep} % Don't apply the gap to beginning and end of lists

% Font declaration and title settings
\setmainfont{Constantia}		% Set the main font
\newfontfamily\headingfont[]{Calibri}
\titleformat{\chapter}[display]
  {\huge\headingfont}{\chaptertitlename\ \thechapter}{20pt}{\Huge}
\titleformat*{\section}{\LARGE\headingfont}
\titleformat*{\subsection}{\Large\headingfont}
\renewcommand{\maketitlehooka}{\headingfont}

% Spacing after titles {command}{left}{before}{after}[right]
\titlespacing{\chapter}{0pt}{0pt}{0pt}
\titlespacing*{\section}{0pt}{0pt}{0pt}
\titlespacing*{\subsection}{0pt}{0pt}{0pt}

% Make hyperlinks black
\hypersetup{colorlinks=false, linkcolor=black}

\begin{document}

	\begin{titlepage}
		\centering
		\huge Title
	\end{titlepage}
	
	\pagenumbering{roman}

	\renewcommand{\contentsname}{Table of Contents}
	\setcounter{tocdepth}{2}
	\addtocontents{toc}{~\hfill\textbf{Page}\par}
	\tableofcontents
	
	\newpage
	\clearpage
	\pagenumbering{arabic}
	
	\section{Question One}
	\label{questionone}
	
	The fundamental principles for the design and implementation of an ISMS, as given in ISO\slash IEC 27001, are:
	
	\begin{itemize}
	\item Organisational awareness of the need for information security.
	
	\item Assignment of responsibility for information security activities.
	
	\item Managerial commitment to information security.
	
	\item Implementation of appropriate controls as determined through risk analysis.
	
	\item Security viewed as an essential component of information systems.
	
	\item Security incidents actively prevented and\slash or detected.
	
	\item A comprehensive approach to security across the organisation.
	
	\item Continuous assessment of information security requirements, modifying the ISMS as necessary.
	
	\end{itemize}
	
	If an organisation does not continually assess their information security needs, and modify their ISMS as a result of any changes to requirements, it is likely that their security needs will cease to be met. For example, consider an that organisation begins outsourcing part of their process to a third party which needs to access some of the customer organisations information. Without reviewing the ISM issues this creates, it might transpire that the contracted company can access customer data, for which they are not authorised. This potential breach in confidentiality could be avoided if appropriate access controls are implemented, the need for which would be identified through reviewing the risk assessment as part of the continuous review process.
	
	\newpage
	\section{Question Two}
	\label{questiontwo}
	
	\subsection{Example One}
	\label{exampleone}
	
	One example of changing circumstances that would require an organisation to review its risk assessment would be deciding to implement a Bring Your Own Device (BYOD) solution. Whilst there may be clear business benefits for doing so, such as reduced operating overheads and increased productivity through employee use of familiar software and hardware, there are significant risks associated with such an approach.
	
	Sensitive company data could be processed on personal equipment. Unauthorised, and possibly malicious, software residing on personal devices may gain access to the company infrastructure. Although just an example of two potential attack vectors, they represent new threats to information security that need to be assessed.
	
	Reviewing the risk assessment would enable the organisation to capture all of the threats and decide what new physical, procedural and technical controls are required. It may even result in a reversal of the decision if the anticipated costs of protection outweigh the business benefit of implementing a BYOD solution. Failure to review the risk assessment is likely to expose an organisation to many unidentified risks, which if realised could have very damaging consequences.
	
	\subsection{Example Two}
	\label{exampletwo}
	
	Another example of a change in circumstances that would require an organisation to review its risk assessment is a change to legislation. Although not likely to be a regular occurrence, it is a possibility, such as the change in the UK from the Data Protection Act to the General Data Protection Regulations.
	
	Without covering specifics of this new legislation, certain questions need to be asked when any change is on the horizon. This includes determining whether existing controls remain fit for purpose, whether new controls need to be implemented, and if any supplementary training is required, etc. Failure to do so may well result in unwittingly being in breach of the law, with financial and\slash or criminal repercussions as a consequence.
	
	Continuous review and improvement of the risk assessments would identify such issues and give the organisation opportunity to modify their ISMS appropriately, in line with the business need. Doing so helps ensure an ISMS remains fit for purpose.
	
	\newpage
	\section{Question Three}
	\label{questionthree}
	
	An article published on an information security website [@Kirk2016] explains that documents containing confidential medical records belonging to several US Olympic athletes were released after the World Anti-Doping Agency (WADA) Anti-Doping Administration and Management System (ADAMS) was hacked. The ADAMS database, which contains over 264,000 athletes confidential records, has been a target of Russian hacking group Fancy Bear on several occasions over the past few months. In August 2016, WADA admitted one of its member's accounts had been compromised and illegally accessed by using the legitimate user's password, obtained through spear-phishing emails. Other users of the system have reported similar attempts to obtain valid login credentials by way of an email requesting them to click a link with a similar domain name to that of the legitimate public ADAMS portal. The attack that led to this latest disclosure of information is attributed to a compromise of an International Olympic Committee (IOC) member's account, used during the Rio 2016 Olympics. No other data is currently believed to have been compromised.
	
	Determining whether or not WADA has followed the requirements of ISO\slash IEC 27001 is subjective, given the information available. It is worth noting that a brief search of both the WADA [@WADA2016] and ADAMS [@ADAMS2016] websites does not reveal any mention of either being certified in accordance with this standard.
	
	WADA appears to have had an awareness of the need for information security, at least in the recent past, announcing a security breach in August 2016. It is not clear from the article whether the organisation assigned responsibility for information security to anybody nor what level of managerial commitment it has in this regard. Given that WADA is reporting that no other data is believed to have been compromised, it is probably reasonable to assume that they are actively trying to detect security breaches. How they are going about this is not clear, and it is possible that they are relying on being informed of compromises and attempted compromises solely from those users who have received spear-fishing emails.
	
	Taking into account the sensitive nature of the information contained within the ADAMS database, and the high-profile nature of some of its users, an obvious assumption is that WADA takes seriously the security of the data stored within its systems. Users are required to enter a username and password to progress beyond the publicly accessible secure login portal [@ADAMS2016], although this is not the most robust of security measures. It is not possible to determine how comprehensive any additional security measures are, and how much of the organisation they span.
	
	The article doesn't mention whether WADA will review their security procedures in light of these data breaches and attempted attacks. It would be prudent for them to do so, and it is probably a reasonable assumption that some sort of risk re-assessment will be conducted.
	
	Looking in more detail at the controls employed, the only obvious examples are the use of the HTTPS protocol and requiring a username and password to gain access to the ADAMS database. These are both appropriate and common security mechanisms to provide a confidentiality service. A procedural control that could have been implemented would be education on threats, including making available the WADA and ADAMS policies on when and how login credentials are requested. It is unclear whether this was done, but evidence would suggest it unlikely.
	
	In light of neither the WADA nor ADAMS websites displaying any prominent notification of conformity to ISO\slash IEC 27001, I am of the opinion that they did not follow the requirements of the standard. Having considered each of the main aspects of the standard, a degree of security management seems to have taken place. However, if WADA had adhered to the directives in the standard, I would expect to have been able to easily identify this in the article.
	
	\newpage
	\section{References}
\end{document}